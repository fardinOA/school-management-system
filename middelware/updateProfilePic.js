const multer = require('multer');
var fileStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images')
    },
    filename: function (req, file, cb) {
        
        let ex = file.originalname.split('.');

        cb(null, file.fieldname + '-' + Date.now() + '.' + ex[1])
    }
})

const upload = multer({
    storage: fileStorage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
            cb(null, true)
        }
        else {
            cb(null, false)
        }
    }
})

module.exports = upload;