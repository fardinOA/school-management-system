const multer=require('multer');
var fileStorage=multer.diskStorage({
    destination:function (req,file,cb) {
        cb(null,'./files/upload')
    },
    filename:function (req,file,cb) {
        
        let ex=file.originalname.split('.');
       
        cb(null,file.fieldname+'-'+Date.now()+'.'+ex[1])
    }
})

const upload=multer({
    storage:fileStorage,
     fileFilter: (req, file, cb) => {
         if (file.mimetype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || file.mimetype == 'application/pdf') {
            cb(null, true)
        }
        else {
            cb(null, false)
        }
    }
})

module.exports=upload;