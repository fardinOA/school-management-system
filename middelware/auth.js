const jwt=require('jsonwebtoken');
const secreateKey = process.env.JWT_SECREAT_KEY

module.exports=function (req,res,next) {
    const token = req.header('Authorization');
    if (!token) {
        return res.status(401).json({
            message: 'Unauthorized user'
        })
    }

    const verified=jwt.verify(token,secreateKey);
    req.user=verified;
    next();

}