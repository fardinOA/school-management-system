import React from 'react'
import {Link} from 'react-router-dom'
function TeacherSignIn() {
  return (
      <div className="user-signUp-container">
          <div>
              <div className="signUpIcon">
                  <i class="fas fa-sign-in-alt 7x"></i>
              </div>
              <form action="" className="was-validated">
                  <div className="form-group">
                      <label htmlFor="">Full Name</label>
                      <input type="text" className="form-control" required />

                  </div>
                
                  <div className="form-group">
                      <label htmlFor="">Email</label>
                      <input type="email" className="form-control" required />
                  </div>
                  
                  <div className="form-group">
                      <button type="submit" className="btn btn-secondary form-control mt-3 " name="" id="" placeholder="" >Login</button>
                  </div>
              </form>
             
          </div>
        </div>
  )
}

export default TeacherSignIn
