import React from 'react'
import {Link} from 'react-router-dom'
function Student() {
  return (
    <div className="user-container">
      <div className='user-content'>
        <div className="hello-user">
          <div>
            <h1>Hello, Student</h1>
            <p>Welcome to Online School Management System</p>
          </div>
        </div>

        <div className="button-section">
          <div>
            <p><b>You can access various features after Login.</b></p>
            <div>

              <Link className="btn btn-primary btn-lg" to="/studentsignin">Login</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Student
