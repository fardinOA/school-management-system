import React from 'react'
import {Link} from 'react-router-dom'

function Home() {
  return (
    <div className='home-container'>
        <div className="home-content">
          <div>
          <h1>Welcome</h1>
          <button className="hmbtn">Take Addmission</button>
          </div>
        </div>
       
         <ul className="row p-3">
           <li className="card cardStyle col-sm-12 col-lg-4"> 
                <Link to="/admin" className="ancor">
              <div className="admin card-img-top" src="" alt="Admin" ></div>
              <div className="card-body">
                <h4 className="cart-title text-center">Admin</h4>

              </div>
                </Link>
           </li>

          <li className="teacher-li card cardStyle col-sm-12 col-lg-4 ">
            <Link to="teacher" className="ancor">
              <div className="teacher card-img-top" src="" alt="Admin" ></div>
              <div className="card-body">
                <h4 className="cart-title text-center">Teacher</h4>

              </div>
            </Link>
          </li>

          <li className=" card cardStyle col-sm-12 col-lg-4">
            <Link to="student" className="ancor">
              <div className="student card-img-top" src="" alt="Admin" ></div>
              <div className="card-body">
                <h4 className="cart-title text-center">Student</h4>

              </div>
            </Link>
          </li>
       
         </ul>
        
    </div>
  )
}

export default Home
