import React from 'react'
import {Link} from 'react-router-dom'
function AdminSignUp() {
  return (
    <div className="user-signUp-container">
     <div>
        <div className="signUpIcon">
          <i class="fas fa-user-plus 7x"></i>
        </div>
        <form action="" className="was-validated">
          <div className="form-group">
            <label htmlFor="">User Name</label>
            <input type="text" className="form-control" required />
          
          </div>
          <div className="form-group">
            <label htmlFor="">Password</label>
            <input type="password" className="form-control" required/>
          </div>
          <div className="form-group">
            <input type="submit" className="btn btn-secondary form-control mt-3"name="" id="" placeholder="" ></input>
          </div>
        </form>
        <Link className="ancor mL" to="/adminsignin">Already have an account? sign in</Link>
     </div>
    </div>
  )
}

export default AdminSignUp
