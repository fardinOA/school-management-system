import React,{useEffect,useState} from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { viewAllStudents, admitSingleStudent} from '../../redux/admin/adminActions'
function AllStudent({history}) {
    const [selectClass,setSesectClass]=useState('');

    const [name,setName]=useState('');
    const [fatherName,setFatherName]=useState('');
    const [motherName,setMotherName]=useState('');
    const [contact,setContact]=useState('');
    const [currentClass,setCurrentClass]=useState('');
    const [birthDay,setBirthDay]=useState('');
    const [birthMonth,setBirthMonth]=useState('');
    const [birthYear,setBirthYear]=useState('');
    const [studentId, setStudentId]=useState('');

    const [model,setModel]=useState(false);
    const viewAllStudent = useSelector(state => state.viewAllStudent);
    const {loading, allStudent, error } = viewAllStudent;
     
    const dispatch = useDispatch();
    useEffect(()=>{
       console.log(selectClass);
        dispatch(viewAllStudents(selectClass))
        
    }, [selectClass])
    
    // console.log("allStudent",allStudent);
    // console.log(allStudent);
    const add=()=>{
        setModel(true);
        if (allStudent.length < 10) {
            var sid = `0${allStudent.length + 1}`
            setStudentId(sid);
        } else {
            setStudentId(allStudent.length + 1);
        }
    }
    const cancleAddStudent=()=>{
        setModel(false);
    }
    
    const submitAddStudent=(e)=>{
        e.preventDefault();
        var data={
            name,
            farherName: fatherName,
            motherName:motherName,
            contactNumber:contact,
            currentClass,
            dateOfBirth:new Date(birthYear,birthMonth,birthDay),
            studentId
        }

       dispatch(admitSingleStudent(data));

       history.push('/dashboard')
       
    }

  

    var date =new Date().getFullYear();
 
  return (
    <div className="all-student-container">
         
      <div className="row">
          <div className="select col-sm-12 col-lg-6">
                   <h3>See Individul Class</h3>
                  <select value={selectClass} onChange={(e) => setSesectClass(e.target.value)}>
                      <option value="">All Class</option>
                      <option value="01">One</option>
                      <option value="02">Two</option>
                      <option value="03">Three</option>
                      <option value="04">Four</option>
                      <option value="05">Five</option>
                      <option value="06">Six</option>
                      <option value="07">Seven</option>
                      <option value="08">Eight</option>
                      <option value="09">Nine</option>
                      <option value="10">Ten</option>
                  </select>
          </div>
              {
                 selectClass? !model && <div className="col-sm-12 col-lg-6">
                      <button onClick={(e) => add()} className='btn btn-primary mt-2' >Add Student</button>
                  </div>
                :
                      <div className="col-sm-12 col-lg-6">
                          <h2 className='alert alert-primary' >For Adding Student Select The Class First</h2>
                      </div>
              }
      </div>
      {
          model&& <div className='add-student-container'>
             <div className="add-student-content">
                     
                      <form action="" className="was-validated">
                          <div className="add-icon">
                              <i class="fas fa-address-card"></i>
                          </div>
                          <div className="form-group">
                              <label htmlFor="">Full Name</label>
                              <input onChange={(e)=>setName(e.target.value)} className='form-control' type="text" required />
                          </div>

                          <div className="form-group">
                              <label htmlFor="">Father's Name</label>
                              <input onChange={(e)=>setFatherName(e.target.value)} className='form-control' type="text" required />
                          </div>

                          <div className="form-group">
                              <label htmlFor="">Mother's Name</label>
                              <input onChange={(e)=>setMotherName(e.target.value)} className='form-control' type="text" required />
                          </div>

                          <div className="form-group">
                              <label htmlFor="">Contact Number</label>
                              <input onChange={(e)=>setContact(e.target.value)} className='form-control mb-2' type="text" required />
                          </div>
                          <div className="form-group">
                              <label htmlFor="">Student Id</label>
                              <input value={studentId} onChange={(e) => setStudentId(e.target.value)} className='form-control mb-2' type="text" required />
                          </div>
                          <div className="form-group mb-2">
                              <label htmlFor="">Select Class: </label>
                              <select value={selectClass} onChange={(e)=>setCurrentClass(e.target.value)} classclassName='' >
                                  <option>Select Class</option>
                                  <option value="01">One</option>
                                  <option value="02">Two</option>
                                  <option value="03">Three</option>
                                  <option value="04">Four</option>
                                  <option value="05">Five</option>
                                  <option value="06">Six</option>
                                  <option value="07">Seven</option>
                                  <option value="08">Eight</option>
                                  <option value="09">Nine</option>
                                  <option value="10">Ten</option>
                              </select>
                          </div>

                          <div className="form-group">
                              <label htmlFor="">Date Of Birth: </label>
                              <select onChange={(e)=>setBirthDay(e.target.value)} className='' required name="" id="">
                                  <option value="">Select Day</option>
                                  {
                                      [...Array(30).keys()].map(ele => (
                                          <option key={ele + 1} value={ele + 1}>{ele + 1}</option>
                                      ))
                                  }
                              </select>

                              <select onChange={(e)=>setBirthMonth(e.target.value)} className='m-1' required name="" id="">
                                  <option value="">Select Month</option>
                                  {
                                      [...Array(12).keys()].map(ele => (
                                          <option key={ele + 1} value={ele + 1}>{ele + 1}</option>
                                      ))
                                  }
                              </select>

                              <select onChange={(e)=>setBirthYear(e.target.value)} className='' required name="" id="">
                                  <option value="">Select Year</option>
                                  {

                                      [...Array(Number(date - 1900)).keys()].map(ele => (
                                          <option key={ele + 1} value={date - ele}>{date - ele}</option>
                                      ))
                                  }
                              </select>

                          </div>
                                 
                                     
                                 <div className="add-student-buttons">
                                    <button onClick={(e)=>submitAddStudent(e)} className="btn btn-primary">Submit</button>
                                    <button onClick={()=>cancleAddStudent()} className='btn btn-secondary'>Cancle</button>
                                 </div>

                      </form>
             </div>
          </div>
      }
          Total Students: {allStudent && <h3>{allStudent.length}</h3>}
      <table className="table table-striped table-hover text-center">
         
          <thead>
              <tr>
                  <th>Student Id</th>
                  <th>Student Name</th>
                  <th>Student Contact</th>
                  <th>Class</th>
                  <th>Active?</th>
              </tr>
          </thead>
          <tbody>
                  {loading &&<p>Loading...</p>} {error&&<p className='error'>{error}</p>}
                 {
                  allStudent?allStudent.map(student => (
                                      <tr key={student._id}>
                                          <th> {student.studentId} </th>
                                          <th> {student.name} </th>
                                          <th> {student.contactNumber} </th>
                                          <th> {student.currentClass} </th>
                                          <th >
                                             <>
                                  {
                                      student.active ? <p className="active-student"> <i class="fas fa-circle"></i></p> : <p className="inActive-student"> <i class="fas fa-circle"></i></p>
                                  }
                                             </>
                                          </th>
                                      </tr>

                                  ))
                          
                      : <h1>No students found</h1> 
                 }  
          </tbody>
      </table>

    </div>
  )
}

export default AllStudent
