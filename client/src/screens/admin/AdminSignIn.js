import React,{useState,useEffect} from 'react'
import { Link } from 'react-router-dom';
import {useSelector,useDispatch} from 'react-redux'
import {signIn} from '../../redux/admin/adminActions'
function AdminSignIn(props) {
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const userSignIn=useSelector(state=>state.userSignIn);
    const {loading,success, userInfo,error}=userSignIn;
  
    const dispatch = useDispatch()
    
    useEffect(()=>{
       
        if (userInfo) props.history.push('dashboard');
        
    }, [userInfo])

    const subminHandeler=(e)=>{
        e.preventDefault();
        
        dispatch(signIn(userName,password))
    }
   
    return (
        <div className="user-signUp-container">
            <div>
                <div className="signUpIcon">
                    <i class="fas fa-sign-in-alt "></i>
                </div>
                <div>
                    {loading&&<p>Loading...</p>}
                    {error&&<p className='error'>{error}</p>}
                </div>
                <form action="" className="was-validated">
                    <div className="form-group">
                        <label htmlFor="">User Name</label>
                        <input onChange={(e)=>setUserName(e.target.value)} type="text" className="form-control" required />

                    </div>
                    <div className="form-group">
                        <label htmlFor="">Password</label>
                        <input onChange={(e) => setPassword(e.target.value)} type="password" className="form-control" required />
                    </div>
                    <div className="form-group">
                        <button onClick={(e)=>subminHandeler(e)} type="button" className="btn btn-secondary form-control mt-3" name="" id="" placeholder="" >Login</button>
                    </div>
                </form>
                <Link className="ancor mL" to="/adminsignup">Don't have an account? sign up</Link>
            </div>
        </div>
    )
}

// const mapStateToProps = state=>{
//     return {
//         userSignIn:state.userSignIn
//     }
// }

// const mapDispatchToProps =dispatch=>{
//     return{
//         signIn:()=>dispatch(singIn(email,password))
//     }
// }

export default AdminSignIn
