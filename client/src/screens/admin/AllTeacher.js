import React, { useEffect, useState } from 'react'
import {connect} from 'react-redux'
import { viewAllTeacher,admitSingleTeacher } from '../../redux/admin/adminActions'
function AllTeacher(props) {
   

    const [name, setName] = useState('');
    const [contact, setContact] = useState('');
    const [teacherId, setTeacherId] = useState('');
    const [email, setEmail] = useState('');

    const [model, setModel] = useState(false);
  
    const { loading, allTeacher, error } = props.viewAllTeacher;

    useEffect(() => {

    }, [])

    // console.log("allStudent",allStudent);
    // console.log(allStudent);
    const add = () => {
        setModel(true);
        if (allTeacher.length < 10) {
            var sid = `0${allTeacher.length + 1}`
            setTeacherId(sid);
        } else {
            setTeacherId(allTeacher.length + 1);
        }
    }
    const cancleAddTeacher = () => {
        setModel(false);
    }

    const submitAddTeacher = (e) => {
        e.preventDefault();
        var data = {
            name,
            contactNumber:contact,
            teacherId,
            email
        }
        // dispatch(admitSingleTeacher(data))
        props.admitSingleTeacher(data)
        props.history.push('/dashboard')
    }
   

    return (
        <div className="all-student-container">
                {
                    !model && <div className="col-sm-12 col-lg-6">
                        <button onClick={(e) => add()} className='btn btn-primary mt-2' >Add Teacher</button>
                    </div>
                }
            {/* </div> */}
            {
                model && <div className='add-teacher-container'>
                    <div className="add-teacher-content">

                        <form action="" className="was-validated">
                            <div className="add-icon">
                                <i class="fas fa-address-card"></i>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Full Name</label>
                                <input onChange={(e) => setName(e.target.value)} className='form-control' type="text" required />
                            </div>

                          
                            <div className="form-group">
                                <label htmlFor="">Contact Number</label>
                                <input onChange={(e) => setContact(e.target.value)} className='form-control mb-2' type="text" required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Email</label>
                                <input onChange={(e) => setEmail(e.target.value)} className='form-control mb-2' type="email" required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Teacher Id</label>
                                <input value={teacherId} onChange={(e) => setTeacherId(e.target.value)} className='form-control mb-2' type="text" required />
                            </div>
                            
                            <div className="add-student-buttons">
                                <button onClick={(e) => submitAddTeacher(e)} className="btn btn-primary">Submit</button>
                                <button onClick={() => cancleAddTeacher()} className='btn btn-secondary'>Cancle</button>
                            </div>

                        </form>
                    </div>
                </div>
            }
            Total Teacher: {allTeacher && <h3>{allTeacher.length}</h3>}
            <table className="table table-striped table-hover text-center">

                <thead>
                    <tr>
                        <th>Teacher Id</th>
                        <th>Teacher Name</th>
                        <th>Teacher Contact</th>
                        <th>Active?</th>
                    </tr>
                </thead>
                <tbody>
                    {loading && <p>Loading...</p>} {error && <p className='error'>{error}</p>}
                    {
                        allTeacher ? allTeacher.map(teacher => (
                            <tr key={teacher._id}>
                                <th> {teacher.teacherId} </th>
                                <th> {teacher.name} </th>
                                <th> {teacher.contactNumber} </th>                            
                                <th >
                                    <>
                                        {
                                            teacher.active ? <p className="active-student"> <i class="fas fa-circle"></i></p> : <p className="inActive-student"> <i class="fas fa-circle"></i></p>
                                        }
                                    </>
                                </th>
                            </tr>

                        ))

                            : <h1>No students found</h1>
                    }
                </tbody>
            </table>

        </div>
    )
}

const mapStateToProps = (state) =>{
    return{
        viewAllTeacher:state.viewAllTeacher
    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        admitSingleTeacher:(data)=>dispatch(admitSingleTeacher(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllTeacher)
