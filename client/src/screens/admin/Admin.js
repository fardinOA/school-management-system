import React,{useEffect} from 'react'
import {Link} from 'react-router-dom'
import {useSelector,useDispatch} from 'react-redux'
function Admin(props) {
  const userSignIn = useSelector(state => state.userSignIn);
  const { loading, success, userInfo, error } = userSignIn;
  const dispatch = useDispatch()

  useEffect(() => {
    console.log(userInfo);
    if (userInfo) props.history.push('dashboard');

  }, [userInfo])
  return (

    
    <div className="user-container">
      <div className='user-content'>
        <div className="hello-user">
                  <div>
                      <h1>Hello, Admin</h1>
                      <p>Welcome to Online School Management System</p>
                  </div>
        </div>
                
          <div className="button-section">
                  <div>
                      <p><b>You can access various features after Login/SignUp.</b></p>
                      <div>
              <Link className="btn btn-primary m-2" to="/adminsignup">Sign Up</Link>
             <Link className="btn btn-primary" to="/adminsignin">Login</Link>
                      </div>
                  </div>
          </div>
      </div>
    </div>
  )
}

export default Admin
