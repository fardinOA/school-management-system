import React from 'react'
import cookie from 'js-cookie'
import {Link} from 'react-router-dom'
import {viewAllStudents,viewAllTeacher} from '../../redux/admin/adminActions'
import {useSelector,useDispatch} from 'react-redux'
function AdminDashBoard(props) {

    const dispatch = useDispatch();
    const logOut = (e) => {
        e.preventDefault();
        cookie.remove("userInfo")

        props.history.push('/')
        window.location.reload();
    }

    const viewStudent=()=>{
       dispatch(viewAllStudents());
    }
  const viewTeacher = () => {
    dispatch(viewAllTeacher());
  }
  return (
    <div className='admin-dashboard p-3'>
      
       
       <div className='ul-container'>
         <ul className='row'>
          <li className="card col-sm-12 col-lg-4"><Link onClick={(e)=>viewStudent()} to='/all-student' className="ancor ">View all Student</Link></li>
          <li className="card col-sm-12 col-lg-4"><Link onClick={(e) => viewTeacher()} to='/all-teacher' className="ancor">View all Teacher</Link></li>
          <li className="card col-sm-12 col-lg-4"><Link className="ancor " to="/all-classes" >View all Class</Link></li>
          <li className="card col-sm-12 col-lg-4"><Link className="ancor ">View all Routine</Link></li>
          <li className="card col-sm-12 col-lg-4"><Link className="ancor ">Create Announcement</Link></li>
          
         </ul>
       </div>

      <br /><br />

      <button className='btn btn-primary md-5' onClick={(e)=>logOut(e)}>Log Out</button>

    </div>
  )
}

export default AdminDashBoard
