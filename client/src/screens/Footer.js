import React from 'react'

function Footer() {
    const date=new Date().getFullYear();
  return (
    <footer className="footer bg-dark text-light p-4">
       <div>
        {"Copyright @, "}{date}{" >School Management"}
       </div>
    </footer>
  )
}

export default Footer
