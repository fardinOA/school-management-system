import React from 'react'

import {Link} from 'react-router-dom'



function NavBar() {
   
  return (
    <nav className="navbar styleByMe navbar-expand-md navbar-dark bg-dark sticky-top text-center p-3">
       
      <Link to="/" className="navbar-brand justify-content-center">School Management </Link>
      <button class="navbar-toggler nav-button" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="text-light navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav m-auto">
                       <li className="nav-item"> <Link className="nav-link" to='/'>Home</Link> </li>
                       <li className="nav-item"> <Link className="nav-link" to='/admin'>Admin</Link> </li>
                       <li className="nav-item">  <Link className="nav-link" to='/teacher'>Teacher</Link> </li>
                       <li className="nav-item"> <Link className="nav-link" to='/student'>Student</Link> </li>
                       <li className="nav-item"> <Link className="nav-link">Contact Us</Link> </li>
                       <li className="nav-item"> <Link className="nav-link">About Us</Link> </li>
            </ul>
       </div>
    </nav>
  )
}

export default NavBar



