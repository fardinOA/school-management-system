import React from 'react'
import {Link} from 'react-router-dom'
function Teacher() {
  return (
    <div className="user-container">
      <div className='user-content'>
        <div className="hello-user">
          <div>
            <h1>Hello, Teacher</h1>
            <p>Welcome to Online School Management System</p>
          </div>
        </div>

        <div className="button-section">
          <div>
            <p><b>You can access various features after Login.</b></p>
            <div>
              
              <Link className="btn btn-primary btn-lg" to="/teachersignin">Login</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Teacher
