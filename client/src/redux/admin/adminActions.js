import axios from 'axios';
import {
    USER_REGISTER_REQUEST,USER_REGISTER_SUCCESS,USER_REGISTER_FAIL,
    VIEW_ALL_STUDENT_FAIL,VIEW_ALL_STUDENT_SUCCESS,VIEW_ALL_STUDENT_REQUEST,
    ADMIT_SINGLE_STUDENT_REQUEST,ADMIT_SINGLE_STUDENT_SUCCESS,ADMIT_SINGLE_STUDENT_FAIL,
    ADMIT_SINGLE_TEACHER_REQUEST, ADMIT_SINGLE_TEACHER_SUCCESS, ADMIT_SINGLE_TEACHER_FAIL,
    VIEW_ALL_TEACHER_FAIL, VIEW_ALL_TEACHER_SUCCESS, VIEW_ALL_TEACHER_REQUEST,
    VIEW_ALL_STUDENT_BY_CLASS_SUCCESS
    

} from './adminConstant';
import cookie from 'js-cookie'

const signIn=(userName,password)=>async(dispatch)=>{
    
    dispatch({type:USER_REGISTER_REQUEST,payload:{userName,password}});
    
    try{
        
        const { data } = await axios.post('http://localhost:5050/admin/login',{userName,password});
        console.log(data.message);
        if (data.message == 'Login successfull'){
        dispatch({type:USER_REGISTER_SUCCESS,payload:data})
        cookie.set('userInfo',JSON.stringify(data))
       }
        else dispatch({ type: USER_REGISTER_FAIL, payload: data.message })
        
    }
    catch(error){
        dispatch({ type: USER_REGISTER_FAIL, payload: error.message})
    }
}

const viewAllStudents=(selectClass) =>async(dispatch,getState) =>{
    dispatch({ type: VIEW_ALL_STUDENT_REQUEST });
   const {userSignIn:{userInfo}}=getState();
    const { viewAllStudent: { allStudent }}=getState();

    try {

        if(selectClass){
            console.log("cation",selectClass);
            const { data } = await axios.get('http://localhost:5050/admin/all-student', {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-type": "Application/json",
                    "Authorization": userInfo.token
                }
            });

            dispatch({ type: VIEW_ALL_STUDENT_BY_CLASS_SUCCESS, payload: data.student ,selectClass})
        }

       else {
            const { data } = await axios.get('http://localhost:5050/admin/all-student', {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-type": "Application/json",
                    "Authorization": userInfo.token
                }
            });


            dispatch({ type: VIEW_ALL_STUDENT_SUCCESS, payload: data.student })

            cookie.set('allStudent', data)
       }
       
       
    }
    catch (error) {
        console.log(error);
        dispatch({ type: VIEW_ALL_STUDENT_FAIL, payload: error.message })
    }
}

const admitSingleStudent=(student)=>async(dispatch,getState)=>{
    dispatch({ type:ADMIT_SINGLE_STUDENT_REQUEST, payload: student})
    const { userSignIn: { userInfo } } = getState();
    

    try {
        const { data } = await axios.post('http://localhost:5050/admin/student/admit-single-student',student,{
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-type": "Application/json",
                "Authorization": userInfo.token
            }
        })
      
        dispatch({ type:ADMIT_SINGLE_STUDENT_SUCCESS, payload:data})
       
    } catch (error) {
        dispatch({ type:ADMIT_SINGLE_STUDENT_FAIL, payload:error.message})
    }
}

const viewAllTeacher = () => async (dispatch, getState) => {
    dispatch({ type: VIEW_ALL_TEACHER_REQUEST });
    const { userSignIn: { userInfo } } = getState();
 
    try {

        const { data } = await axios.get('http://localhost:5050/admin/view-all-teacher', {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-type": "Application/json",
                "Authorization": userInfo.token
            }
        });

           
        dispatch({ type: VIEW_ALL_TEACHER_SUCCESS, payload: data.teachers })
       
        cookie.set('allTeacher', data)


    }
    catch (error) {
        console.log("error",error);
        dispatch({ type: VIEW_ALL_TEACHER_FAIL, payload: error.message })
    }
}

const admitSingleTeacher = (teacher) => async (dispatch, getState) => {
    dispatch({ type: ADMIT_SINGLE_TEACHER_REQUEST, payload: teacher })
    const { userSignIn: { userInfo } } = getState();


    try {
        const { data } = await axios.post('http://localhost:5050/admin/teacher/create', teacher, {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-type": "Application/json",
                "Authorization": userInfo.token
            }
        })

        dispatch({ type: ADMIT_SINGLE_TEACHER_SUCCESS, payload: data })
       

    } catch (error) {
        dispatch({ type: ADMIT_SINGLE_TEACHER_FAIL, payload: error.message })
    }
}


export {signIn,viewAllStudents,admitSingleStudent,viewAllTeacher,admitSingleTeacher}