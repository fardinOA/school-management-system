
import {
    USER_REGISTER_SUCCESS,USER_REGISTER_REQUEST,USER_REGISTER_FAIL,
    VIEW_ALL_STUDENT_FAIL,VIEW_ALL_STUDENT_SUCCESS,VIEW_ALL_STUDENT_REQUEST   ,
    ADMIT_SINGLE_STUDENT_REQUEST,ADMIT_SINGLE_STUDENT_SUCCESS,ADMIT_SINGLE_STUDENT_FAIL,
    VIEW_ALL_TEACHER_REQUEST,VIEW_ALL_TEACHER_SUCCESS,VIEW_ALL_TEACHER_FAIL,
    ADMIT_SINGLE_TEACHER_REQUEST,ADMIT_SINGLE_TEACHER_SUCCESS,ADMIT_SINGLE_TEACHER_FAIL,
    VIEW_ALL_STUDENT_BY_CLASS_SUCCESS
} from './adminConstant';

function userSignInReducer(state={},action) {
    switch(action.type) {
        case USER_REGISTER_REQUEST:
            return {loading: true,success:false}
        case USER_REGISTER_SUCCESS:
            return {loading: false,success:true,userInfo:action.payload};
        case USER_REGISTER_FAIL:
            return {loading: false,success:false,error:action.payload};
        default:
            return state;
    }
}

function viewAllStudentReducer(state={allStudent:[]},action) {
    switch(action.type) {
        case VIEW_ALL_STUDENT_REQUEST:
            return {loading:true};
        case VIEW_ALL_STUDENT_SUCCESS:
            return {loading:false,allStudent:action.payload};
        case VIEW_ALL_STUDENT_FAIL:
            return {loading:false,error:action.payload};
        case VIEW_ALL_STUDENT_BY_CLASS_SUCCESS:
           const arr=action.payload;
           const arr2=[];
           arr.map(element => element.currentClass==action.selectClass&&arr2.push(element));
           return {loading:false,allStudent:arr2}
        default:
            return state;
    }
}

function admitSingleStudentReducer(state={}, action) {
    switch (action.type) {
        case ADMIT_SINGLE_STUDENT_REQUEST:
            return {loadiing:true}
        case ADMIT_SINGLE_STUDENT_SUCCESS:
            return{loading:false,state:action.payload}
        case ADMIT_SINGLE_STUDENT_FAIL:
            return {loading:false,error:action.payload}
        default:
            return state;
    }
}

function viewAllTeacherReducer(state = { allTeacher: [] }, action) {
    switch (action.type) {
        case VIEW_ALL_TEACHER_REQUEST:
            return { loading: true };
        case VIEW_ALL_TEACHER_SUCCESS:
            return { loading: false, allTeacher: action.payload };
        case VIEW_ALL_TEACHER_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
}

function admitSingleTeacherReducer(state = {}, action) {
    switch (action.type) {
        case ADMIT_SINGLE_TEACHER_REQUEST:
            return { loadiing: true }
        case ADMIT_SINGLE_TEACHER_SUCCESS:
            return { loading: false, state: action.payload }
        case ADMIT_SINGLE_TEACHER_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
}

export { userSignInReducer, viewAllStudentReducer, admitSingleStudentReducer, viewAllTeacherReducer, admitSingleTeacherReducer};