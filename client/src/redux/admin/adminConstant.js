

export const USER_REGISTER_REQUEST ="USER_REGISTER_REQUEST"
export const USER_REGISTER_SUCCESS ="USER_REGISTER_SUCCESS"
export const USER_REGISTER_FAIL ="USER_REGISTER_FAIL"

export const VIEW_ALL_STUDENT_REQUEST = "VIEW_ALL_STUDENT_REQUEST"
export const VIEW_ALL_STUDENT_SUCCESS = "VIEW_ALL_STUDENT_SUCCESS"
export const VIEW_ALL_STUDENT_BY_CLASS_SUCCESS = "VIEW_ALL_STUDENT_BY_CLASS_SUCCESS"
export const VIEW_ALL_STUDENT_FAIL = "VIEW_ALL_STUDENT_FAIL"

export const ADMIT_SINGLE_STUDENT_REQUEST = "ADMIT_SINGLE_STUDENT_REQUEST"
export const ADMIT_SINGLE_STUDENT_SUCCESS = "ADMIT_SINGLE_STUDENT_SUCCESS"
export const ADMIT_SINGLE_STUDENT_FAIL = "ADMIT_SINGLE_STUDENT_FAIL"

export const ADMIT_SINGLE_TEACHER_REQUEST = "ADMIT_SINGLE_TEACHER_REQUEST"
export const ADMIT_SINGLE_TEACHER_SUCCESS = "ADMIT_SINGLE_TEACHER_SUCCESS"
export const ADMIT_SINGLE_TEACHER_FAIL = "ADMIT_SINGLE_TEACHER_FAIL"

export const VIEW_ALL_TEACHER_REQUEST = "VIEW_ALL_TEACHER_REQUEST"
export const VIEW_ALL_TEACHER_SUCCESS = "VIEW_ALL_TEACHER_SUCCESS"
export const VIEW_ALL_TEACHER_FAIL = "VIEW_ALL_TEACHER_FAIL"

