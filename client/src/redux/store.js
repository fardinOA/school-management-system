import {createStore,combineReducers,compose,applyMiddleware} from 'redux';
import { userSignInReducer, viewAllStudentReducer, admitSingleStudentReducer, viewAllTeacherReducer, admitSingleTeacherReducer} from './admin/adminReducers'
import thunk from 'redux-thunk'
import cookie from 'js-cookie'

const userInfo = cookie.get('userInfo')||null;
const allStudent = cookie.get('allStudent')||[];
const allTeacher = cookie.get('allTeacher')||[];
const initialState={
    userSignIn:{userInfo},
    viewAllStudent:{allStudent},
    viewAllTeacher:{allTeacher}
}

const allReducers=combineReducers({
    userSignIn:userSignInReducer,
    viewAllStudent: viewAllStudentReducer,
    admitSingleStudent: admitSingleStudentReducer,
    admitSingleTeacher: admitSingleTeacherReducer,
    viewAllTeacher:viewAllTeacherReducer
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENTION_COMPOSE__ || compose
const store = createStore(allReducers, initialState, composeEnhancer(applyMiddleware(thunk)));

export default store;



























