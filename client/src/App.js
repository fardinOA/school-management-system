import logo from './logo.svg';
import './App.css';
import {BrowserRouter,Route,Link} from 'react-router-dom'
import SignIn from './screens/SignIn';
import AdminSignUp from './screens/admin/AdminSignUp';
import Home from './screens/Home';
import NavBar from './screens/NavBar';
// import '../src/style.css'
import './bootstrap_5/bootstrap.min.css'
import './index'
import Footer from './screens/Footer';
import Admin from './screens/admin/Admin';
import Teacher from './screens/Teacher';
import Student from './screens/Student';
import AdminSignIn from './screens/admin/AdminSignIn';
import TeacherSignIn from './screens/TeacherSignIn';
import StudentSignIn from './screens/StudentSignIn';
import AdminDashBoard from './screens/admin/AdminDashBoard';

import store from './redux/store'
import {Provider} from 'react-redux'
import AllStudent from './screens/admin/AllStudent';
import AllTeacher from './screens/admin/AllTeacher';
import AllClass from './screens/admin/AllClass';

function App() {
  return (
  
    <Provider store={store} >
      <BrowserRouter >

        <div className="container app-container">
          <header>
            <NavBar />
          </header>
          <main>
            <Route path="/signin" component={SignIn}></Route>
            <Route path="/" exact={true} component={Home}></Route>
            <Route path="/adminsignup" component={AdminSignUp}></Route>
            <Route path="/adminsignin" component={AdminSignIn}></Route>
            <Route path="/teachersignin" component={TeacherSignIn}></Route>
            <Route path="/studentsignin" component={StudentSignIn}></Route>
            <Route path="/admin" component={Admin}></Route>
            <Route path="/teacher" component={Teacher}></Route>
            <Route path="/student" component={Student}></Route>
            <Route path="/dashboard" component={AdminDashBoard}></Route>
            <Route path="/all-student" component={AllStudent}></Route>
            <Route path="/all-teacher" component={AllTeacher}></Route>
            <Route path="/all-classes" component={AllClass}></Route>
          </main>

          <footer>
            <Footer />
          </footer>
        </div>

      </BrowserRouter>
    </Provider>
    
  );
}

export default App;



// <div className="">
//   <NavBar className=""></NavBar>
//   <div className="main">
//     <Route path="/signin" component={SignIn}></Route>
//     <Route path="/" exact={true} component={Home}></Route>
//     <Route path="/signup" component={SignUp}></Route>
//     <Route path="/admin" component={Admin}></Route>
//     <Route path="/teacher" component={Teacher}></Route>
//     <Route path="/student" component={Student}></Route>
//   </div>
//   <Footer className=""></Footer>
// </div>