const mongoose=require('mongoose');
const bcrypt=require('bcrypt');
const Schema=mongoose.Schema;

var year=new Date().getFullYear();
var teacherCount=1;

const teacherSchema=new Schema({
    name:{
        type:String,
        required:true
    },
    educationalQulification:{
       msc:{
           university:String,
           grade:String,
           passingYear:String
       },
       bsc:{
           university: String,
           grade: String,
           passingYear: String
       },
       hsc:{
           college: String,
           grade: String,
           passingYear: String
       }
    },
    contactNumber: {
        type: String,
       
    },
    email:{
        type:String,
        trim:true,
        unique:true
    },
    teacherId: {
        type: String,
        default:'',
        fixed:true,
        
    },
    password: {
        type: String,
        default: "1234"
    },
    active: {
        type: Boolean,
        default: true
    },
    resetLink: {
        type: String,
        default: ''
    },
    isDeleted: {
        type: Boolean,
        default: false
    }


})


teacherSchema.pre('save', function (next) {
    var user = this;
    if (user.isNew) {
        const id = year + '7410' + user.teacherId;
        if (id) {
            user.teacherId = id;
        }
        next();
    } else {
        next();
    }

})

teacherSchema.pre('save', function (next) {
    var user = this;
    if (user.isModified('password') || user.isNew) {
        bcrypt.genSalt(10, function (err, saltValue) {
            if (err) {
                return next(err)
            }
            bcrypt.hash(user.password, saltValue, function (err, hash) {
                if (err) {
                    return next(err);
                }
                if (hash) {
                    user.password = hash;
                }
                next();
            })
        })
    } else {
        next();
    }

})

module.exports=mongoose.model('Teacher',teacherSchema);