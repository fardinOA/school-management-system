const mongoos=require('mongoose');
const bcrypt=require('bcrypt')
const Schema=mongoos.Schema;

const AdminSchema=new Schema({
    userName:{
        type:String,
        required:true,
        unique:true
    },
    password: {
        type: String,
        required: true,
        min:6,
        max:20
    },
    userType:{
        type:String,
        default:'admin',
        fixed:true
    },
    isDeleted:{
        type:Boolean,
        default:false
    }
})

AdminSchema.pre('save', function (next) {
    var user = this;
    if (user.isModified('password') || user.isNew) {
        bcrypt.genSalt(10, function (err, saltValue) {
            if (err) {
                return next(err)
            }
            bcrypt.hash(user.password, saltValue, function (err, hash) {
                if (err) {
                    return next(err);
                }
                if (hash) {
                    user.password = hash;
                }
                next();
            })
        })
    } else {
        next();
    }

})



module.exports=mongoos.model('Admin',AdminSchema);