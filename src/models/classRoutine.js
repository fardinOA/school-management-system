const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const routineSchema=new Schema(
  {
      className:{
          type:String,
          required:true,
          unique:true
      },
      routine:[
          {
              courseCode: {
                  type: String,
                  required: true
              },
              courseTitle: {
                  type: String,
                  required: true
              },
              teacher: {
                  type: String

              },
              classTime: {
                  time: String,
                  day: String
              }
          }
      ]
  }
)
   
module.exports=mongoose.model('Routine',routineSchema)