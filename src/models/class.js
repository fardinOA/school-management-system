const mongoose=require('mongoose');
const student = require('./student');
const Schema=mongoose.Schema;

const classSchema=new Schema({
    className:{
        type:String,
        required:true
    },
    classStudents:[ 
        String
     ],
    isDeleted: {
        type: Boolean,
        default: false
    }
})


module.exports=mongoose.model('Class',classSchema);

