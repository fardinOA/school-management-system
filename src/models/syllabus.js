const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const syllabusSchema=new Schema({
    subjectName:{
        type:String,
        require:true,
        unique:true
    },
    className:String,
   fileName:String
})



module.exports=mongoose.model('Syllabus',syllabusSchema);


// hotelSchema.index({
//     slug: 1,
//     'metaData.title': 1,
//     'address.division': 1,
//     'address.district': 1,
//     'address.upazila': 1,
//     'address.area': 1,
//     'address.address': 1


// });