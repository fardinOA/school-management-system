const mongoose=require('mongoose');
const bcrypt=require('bcrypt')
const Schema=mongoose.Schema;
var studentCount=0;

var year=new Date().getFullYear();

const studentSchema=new Schema({
    name:{
        type:String,
        required:true
    },
    farheName: {
        type: String,
        required: true
    },
    motherName: {
        type: String,
        required: true
    },
    dateOfBirth:{
        type:Date,
        required:true
    },
    address: {
        division: String,
        district: String,
        upazila: String,
        zipCode: String,
        area: String,
        
    },
    contactNumber:{
        type:String,
        required:true
    },
    gender: {
        type: String,
        enum: ["male", "female","other"],
        required:true
    },
    currentClass:{
        type:String,
        required:true
    }
    ,
    studentId:{
        type:String,
        default:'',
        fixed:true,
        unique:true
    },
    email:{
        type:String,
        trim:true,
        unique:true
    },
    password:{
        type:String,
        default:"1234"
    },
    active:{
        type:Boolean,
        default:true
    },
    resetLink: {
        type: String,
        default: ''
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    profilePic:String

})

studentSchema.pre('save', function (next) {
    var user = this;
    if (user.isNew) {
       const id=year+''+user.currentClass+''+user.studentId;
       if(id){
           user.studentId=id;
       }
       next();
    } else {
        next();
    }

})

studentSchema.pre('save', function (next) {
    var user = this;
    if (user.isModified('password') || user.isNew) {
        bcrypt.genSalt(10, function (err, saltValue) {
            if (err) {
                return next(err)
            }
            bcrypt.hash(user.password, saltValue, function (err, hash) {
                if (err) {
                    return next(err);
                }
                if (hash) {
                    user.password = hash;
                }
                next();
            })
        })
    } else {
        next();
    }

})


module.exports=mongoose.model('Student',studentSchema);