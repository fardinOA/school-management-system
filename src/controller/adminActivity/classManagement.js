const Class=require('../../models/class');
const { findById, findByIdAndUpdate } = require('../../models/student');
const Student=require('../../models/student')

const createClass=async(req,res)=>{
    try{
        const CLASS=new Class({
            ...req.body
        })
        await CLASS.save();
        return res.json({
            message:'Class created successfull'
        })
    }
    catch(err){
        res.json({err})
    }
}

const updateClass=async(req,res)=>{
    try{
        const _id=req.params.id;
       
        const arr =req.body.classStudents;
        var valid;
        for(var i=0;i<arr.length;i++){
            var studentID=arr[i];
           valid= await Student.findOne({isDeleted:false,studentId:studentID})
             
           if(!valid){
               res.status(404).json({
                   message:'Not a valid student you added'
               })
           }
        }
       
        await Class.findByIdAndUpdate(
            {_id},
            {
                $push:req.body
            },
            {
                multi:true
            }
        )

        return res.json({
            message:'update successfull'
        })
    }
    catch(err){
        res.json({err})
    }
}

const deleteClass=async (req,res)=>{
    try{
       const _id=req.params.id;
       await Class.findByIdAndUpdate(
           {_id},
           {
               $set: { isDeleted:true}
           },
           {
               multi:true
           }
       )
       return res.json({
           message:'Successfully deleted'
       })
    }
    catch(err){
       
        res.json({err})
    }
}



const viewAllClass=async(req,res)=>{
    try{
        const CLASS= await Class.find({isDeleted:true});
        return res.json({
            message:'All Classess',
            CLASS
        })
    }
    catch(err){
        res.json({err})
    }
}



module.exports={
    createClass,
    updateClass,
    deleteClass,
    viewAllClass
}