const Admin=require('../../models/admin');
const bcrypt=require('bcrypt');
const jwt=require('jsonwebtoken')
const decode=require('jwt-decode');

const secreateKey = process.env.JWT_SECREAT_KEY


const adminRegisterController=async(req,res)=>{
    try{
        const {userName, password}=req.body;
      
       // const hashPassword= await bcrypt.hash(password,10);
       
        let admin=new Admin({
            userName,
            password
        })

        let isSave=await admin.save();
        if(isSave){
            res.status(201).json({
                message:'Admin create successfully'
            })
        }
        else{
            res.json({
                message:'Can not create an Admin'
            })
        }
    }
    catch(err){
        res.json({err});
    }
}

const loginController=async(req,res)=>{
    try{
        const {userName,password}=req.body;

        let admin = await Admin.findOne({ userName, isDeleted:false});

        let data={
            userName:admin.userName
        }
        if(admin){
            let comp=await bcrypt.compare(password,admin.password);
            const token=jwt.sign(data,secreateKey,{expiresIn:'1h'})
            if(comp){
                res.status(200).json({
                    message:'Login successfull',
                    token
                })
            }
            else{
                res.json({
                    message:'password does not match'
                })
            }
        }
        else{
            res.json({
                message:'User not found'
            })
        }

    }
    catch(err){
        res.json({err});
    }
}

const changePassword=async(req,res)=>{
    try{
        const {courrentPassword,newPassword,confirmNewPassword}=req.body;
        const _id=req.params.id;
        
        const admin = await Admin.findById({ _id, isDeleted:false});
        if(admin){
            
            const ck=await bcrypt.compare(courrentPassword,admin.password);
            if(ck){
                const ck2 = await bcrypt.compare(newPassword, admin.password);
                if (ck2) {
                    res.json({
                        message: 'you are changing you password to your current password'
                    })
                }

                if(newPassword===confirmNewPassword){
                    const hashPassword=await bcrypt.hash(newPassword,10);
                    await Admin.findByIdAndUpdate(
                        { _id },
                        {
                            $set: { password: hashPassword }
                        },
                        {
                            multi: true
                        }
                    )

                    return res.json({
                        message:'password change successfully'
                    })
                }
                else{
                    res.json({
                        message:"nwe password & confirm password does not match"
                    })
                }
            }
            else{
                res.json({
                    message:'Your current password does not match'
                })
            }
            
        }
        else{
            res.json({
                message:'use not found'
            })
        }

    }
    catch(err){
        res.json({err})
    }
    
}



module.exports={
    adminRegisterController,
    loginController,
    changePassword
}