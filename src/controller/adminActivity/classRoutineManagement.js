
const Class=require('../../models/class')
const Routine=require('../../models/classRoutine');


const createRoutine=async(req,res)=>{
    try{
        const _id=req.params.classId;
        const CLASS=await Class.findById({isDeleted:false,_id});
        console.log(req.body);
        let routine=new Routine({
            className:CLASS.className,
            routine:[{...req.body}]
        })
       let isSave= await routine.save();
       if(isSave){
           return res.json({
               message: 'routine created successfull'

           }) 
       }else{
           res.json({
               message:'can not create a routine'
           })
       }
       
    }
    catch(err){
        console.log(err);
        res.json({err})
    }
}



const updateRoutine=async(req,res)=>{
    try{
        const c_id=req.params.classId;
        const r_id=req.params.routineId;
        const CLASS=await Class.findOne({isDeleted:false,_id:c_id});
        const routine = await Routine.findOne({_id:r_id});
      
        if(CLASS && routine){
            if(CLASS.className===routine.className){
              
               const done= await Routine.findByIdAndUpdate(
                    { _id:r_id },
                    {
                        $push: req.body
                    },
                    {
                        multi: true
                    }
                )
               

            }else{
                res.json({
                    message:'Not same class'
                })
            }
        }else{
            res.json({
                message:'class or routine not found'
            })
        }
        
       
       
        return res.json({
            message:'update successfull'
        })
    }
    catch(err){
        
        res.json({err})
    }
}


const deleteRoutine=async(req,res)=>{
    try{
        const c_id = req.params.classId;
        const r_id = req.params.routineId;
        const CLASS = await Class.findOne({ isDeleted: false, _id: c_id });
        const routine = await Routine.findOne({ _id: r_id });

        if (CLASS && routine) {
            if (CLASS.className === routine.className) {

               await Routine.findByIdAndDelete({_id:r_id})
                return res.json({
                    message:'deleted'
                })

            } else {
                res.json({
                    message: 'Not same class'
                })
            }
        } else {
            res.json({
                message: 'class or routine not found'
            })
        }

    }
    catch(err){
        res.json({err})
    }
}

const viewAllRoutine=async(req,res)=>{
    try{
        const c_id=req.params.classId;
        const CLASS=await Class.findOne({_id:c_id});
        if(CLASS){
            const routines=await Routine.find({className:CLASS.className});
            if(routines){
                res.json({
                    message:'all routines',
                    routines
                })
            }
            else{
                res.json({
                    message:"There is no routine for this class"
                })
            }
        }
        else{
            res.json({
                message:'Class not found'
            })
        }
    }
    catch(err){
        res.json({err})
    }
}



module.exports={
    createRoutine,
    updateRoutine,
    deleteRoutine,
    viewAllRoutine
}