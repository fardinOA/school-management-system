const Student=require('../../models/student');


const AdmitSingleStudent=async(req,res)=>{
    try{
      
        const student=new Student({
            ...req.body
        })
       const isSave= await student.save();
       if(isSave){
           res.status(200).json({
               message:'Admit Student Successfull',
               student
           })
       }else{
           res.json({
               message:"can not admit student"
           })
       }
    }
    catch(err){
        res.json({err})
    }
}


const updateStudentInfo=async(req,res)=>{
    try{
        const _id=req.params.id;
        await Student.findByIdAndUpdate(
            {
                _id,
                isDeleted:false
            },
            {
                $set:req.body
            },
            {
                multi:true
            }

        );
        return res.json({
            message:'Student Info Update successfully'
        })

    }
    catch(err){
        res.json({err});
    }
}

const deleteStudent=async(req,res)=>{
    try{
        const _id=req.params.id;
        await Student.findByIdAndUpdate(
            {_id},
            {
                $set:{isDeleted:true}
            },
            {
                multi:true
            }
        )
        return res.json({
            message:'Deletion Complete'
        })
    }
    catch(err){
        res.json({err});
    }
}

const changeActiveInActiveStatus=async(req,res)=>{
    try{
        const _id=req.params.id;
        const value=req.params.value;
        var isActive;
        if(value==='false')isActive=false;
        else if(value==='true') isActive=true;
        
        await Student.findByIdAndUpdate(
            {_id},
            {
                $set:{active:isActive}
            },
            {
                multi:true
            }
        )
        var s='';
        if(isActive) s+='Active';
        else s+='Incative';
        return res.json({
            message:`change to ${s}`
        })
    }
    catch(err){
        res.json({err});
    }
}

const viewProfile=async(req,res)=>{
    try{
        const _id=req.params.id;
        const student=await Student.findOne({_id,isDeleted:false});
        if(student){
            res.json({
                message:'Student',
                student
            })
        }
        else{
            res.json({
                message:'Student no found'
            })
        }
    }
    catch(err){
        res.json({err});
    }
}


module.exports={
    AdmitSingleStudent,
    updateStudentInfo,
    deleteStudent,
    changeActiveInActiveStatus,
    viewProfile
}