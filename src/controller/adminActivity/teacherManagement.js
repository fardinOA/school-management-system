const Teacher=require('../../models/teacher');

const createNewTeacher=async(req,res)=>{
    try{
        const teacher = new Teacher({
            ...req.body
        })
        const isSave = await teacher.save();
        if (isSave) {
            res.status(200).json({
                message: 'create teacher Successfull',
                teacher
            })
        } else {
            res.json({
                message: "can not create a teacher"
            })
        }
    }
    catch(err){
        
        res.json({err});

    }
}


const updateTeacherInfo=async(req,res)=>{
    try{
         const _id=req.params.id;
         await Teacher.findByIdAndUpdate(
             {
                 _id
             },
             {
                 $set:req.body
             },
             {
                 multi:true
             }
         )
         return res.json({
             message:'Update successfull'
         })
    }
    catch(err){
        res.json({err});
    }
}



const deleteTeacher=async(req,res)=>{
    try{
        const _id=req.params.id;
        await Teacher.findByIdAndUpdate(
            {_id},
            {
                $set:{isDeleted:true}
            },
            {
                multi:true
            }
        )

        return res.json({
            message:'Delete successfull'
        })
    }
    catch(err){
        res.json({
            err
        })
    }
}


const changeActiveInActiveStatus=async(req,res)=>{
    try{
        const _id=req.params.id;
        const value=req.params.value;
        var isActive;
        var s='';
        if(value==='true') {isActive=true;s+='Active';}
        else if(value==='false') {isActive=false;s+="InActive";}
        await Teacher.findByIdAndUpdate(
            {_id},
            {
                $set:{active:isActive}
            },
            {
                multi:true
            }
        )
        var s;
        
        return res.json({
            message:`change active status to ${s}`
        })
    }
    catch(err){
        res.json({err})
    }
}


module.exports={
    createNewTeacher,
    updateTeacherInfo,
    deleteTeacher,
    changeActiveInActiveStatus
}