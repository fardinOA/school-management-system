const Student=require('../../models/student');
const Teacher=require('../../models/teacher');
const Routine=require('../../models/classRoutine');
const Syllabus=require('../../models/syllabus');
const jwt=require('jsonwebtoken');
const bcrypt=require('bcrypt');
const jwt_decode=require('jwt-decode');
const path=require('path');
const fs=require('fs')

const secreateKey = process.env.JWT_SECREAT_KEY

const studentLogin=async(req,res)=>{
    try{
        const {studentId,password}=req.body;
        const student = await Student.findOne({studentId,isDeleted:false});
        if(student){
            
            let data={
                studentId:student.studentId,
                currentClass: student.currentClass
            }
            const token=jwt.sign(data,secreateKey,{expiresIn:'1h'});
            const comp=await bcrypt.compare(password,student.password);
            if (comp) {
                res.status(200).json({
                    message: 'Login successfull',
                    token
                })
            }
            else {
                res.json({
                    message: 'password does not match'
                })
            }
        }
        else {
            res.json({
                message: 'User not found'
            })
        }
    }
    catch(err){
        res.json({err});
    }
}

const viewProfile=async(req,res)=>{
    try{
        const token = req.header('Authorization');
        const decode=jwt_decode(token);
        const currentClass = decode.currentClass;
        const studentId = decode.studentId
        console.log(studentId);
        const student = await Student.findOne({ studentId,currentClass,isDeleted:false});
        
        
        let studentView={
            Name:student.name,
            FatherName: student.fatherName,
            MotherName: student.motherName,
            Phone: student.contatcNumber,
            Class: student.currentCalss
        }
        if(student){
            res.json({
                studentView
            })
        }
    }
    catch(err){
       
        res.json({err});
    }
}

const viewAllTeacher=async(req,res)=>{
    try{
        const teacher=await Teacher.find({isDeleted:false});
       
        if(teacher){
            let teacherView = {
                Name: teacher.name,
                ID: teacher.teacherId
            }
            res.json({
                teacherView
            })
        }
    }
    catch(err){
        res.json({err});
    }
}


const viewSingleTeacher = async (req, res) => {
    try {
        const{teacherId}=req.body;
        const teacher = await Teacher.find({teacherId, isDeleted: false });

        if (teacher) {
            let teacherView = {
                Name: teacher.name,
                ID: teacher.teacherId,
                phone: teacher.contactNumber,
                Email:teacher.email
            }
            res.json({
                teacherView
            })
        }
    }
    catch (err) {
        res.json({ err });
    }
}


const viewClassRoutine=async(req,res)=>{
    try{
        const token = req.header('Authorization');
        const decode=jwt_decode(token);
        const className = decode.currentClass;
        const routine=await Routine.findOne({className});
        if(routine){
            res.json({
                routine
            })
        }
    }
    catch(err){
        res.json({err})
    }
}


const viewSyllabus=async(req,res)=>{
 
        try {
            const token = req.header('Authorization');
            const decode = jwt_decode(token);
            const className = decode.currentClass;
            const syllabus = await Syllabus.findOne({ className });
            if (syllabus) {
                res.json({
                    syllabus
                })
            }
        }
        catch (err) {
            res.json({ err })
        }
  
}

const downloadSyllabus=async(req,res)=>{
    try {
        const token = req.header('Authorization');
        const decode = jwt_decode(token);
        const className = decode.currentClass;
        const syllabus = await Syllabus.findOne({ className });
        
        if(syllabus){
            const fileName = syllabus.fileName;
            var filePath = path.join('./files/upload', fileName);

            res.download(filePath, fileName, function (error) {
                if (error) {
                    res.json({
                        message: 'file could not be downloaded'
                    })
                }
            });
        }
    } catch (error) {
        res.json({error})
    }
}


const changePassword = async (req, res) => {
    try {
        const _id = req.params.id;
        const { currentPassword, newPassword, confirmNewPassword } = req.body;

        const student = await Student.findById({ _id });
        if (student) {
            const comp = await bcrypt.compare(currentPassword, student.password);
            if (comp) {
                const comp2 = await bcrypt.compare(newPassword, student.password);
                if (comp2) {
                    res.json({
                        message: 'you are changing you current password to current password'
                    })
                }
                if (newPassword === confirmNewPassword) {
                    const hashPassword = await bcrypt.hash(newPassword, 10);
                    await Student.findByIdAndUpdate(
                        { _id },
                        {
                            $set: { password: hashPassword }
                        },
                        {
                            multi: true
                        }

                    )
                    res.json({
                        message: 'password changed'
                    })
                }

            }
            else {
                res.json({
                    message: 'password does not match'
                })
            }
        }
    } catch (error) {
        res.json({ error })
    }
}



const forgotPassword = async (req, res) => {
    try {
        const { email } = req.body;
        const student = await Student.findOne({ email });
        if (!student) {
            res.status(400).json({
                message: 'user not found'
            })
        }

        const token = jwt.sign({ _id: student._id }, secreateKey, { expiresIn: '5m' });
        await Student.updateOne({ resetLink: token });

        const Email = process.env.NM_EMAIL;
        const Pass = process.env.NM_EMAIL_PASS;


        //node mailer setup
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: Email,
                pass: Pass
            }
        })


        let mailOption = {
            from: 'mdrabby7410@gmail.com',
            to: user.email,
            subject: 'Reset Password Link',
            text: token

        }

        transporter.sendMail(mailOption, function (err, data) {
            if (err) {
                res.json({
                    message: 'can not send mail',
                    err
                })
            }
            else {
                return res.json({
                    message: 'mail send successfully'
                })
            }
        })


    } catch (error) {
        res.json({ error })
    }
}



const resetPassword = async (req, res) => {
    try {
        const secrateKey = process.env.JWT_SECRETE
        const { token, newPassword } = req.body;
        var hashPassword = await bcrypt.hash(newPassword, 10);
        const isTrue = jwt.verify(token, secrateKey);

        if (isTrue) {
            const student = await Student.findOne({ resetLink: token });
            if (student) {
                await Student.updateOne({
                    password: hashPassword,
                    resetLink: ''
                })
                return res.json({
                    msg: 'Password Reset Successfully'
                })
            }
            else {
                res.json({
                    msg: 'Invalid token or expired'
                })
            }

        }
        else {
            res.json({
                msg: 'user not found fot this token'
            })
        }

    } catch (err) {
        res.json({ err })
    }
}


const updateProfilePic = async (req, res) => {
    try {

        const _id = req.params.id;
        const student = await Student.findById({ _id });
        if (student) {

            if (student.profilePic) {
                var profilePic = student.profilePic;
                var filePath = path.join('./files/images', profilePic);
                fs.unlinkSync(filePath);
            }

            await Student.findByIdAndUpdate(
                { _id },
                {
                    $set: { profilePic: req.file.filename }
                },
                {
                    multi: true
                }
            )
            res.json({
                message: 'file upload successfully'
            })

        }
    }
    catch (err) {
        console.log(err);

        res.json({ err })
    }
}




module.exports={
    studentLogin,
    viewProfile,
    viewAllTeacher,
    viewSingleTeacher,
    viewClassRoutine,
    viewSyllabus,
    downloadSyllabus,
    changePassword,
    forgotPassword,
    resetPassword,
    updateProfilePic
}