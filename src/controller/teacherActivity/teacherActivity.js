
const Syllabus=require('../../models/syllabus');
const Student=require('../../models/student');
const Teacher=require('../../models/teacher')
const path=require('path');
const bcrypt=require('bcrypt');
const jwt=require('jsonwebtoken')
const fs=require('fs');
const nodemailer=require('nodemailer');
const secreateKey = process.env.JWT_SECREAT_KEY



const createSyllabus=async(req,res)=>{
    try{
        
        const syllabus=new Syllabus({
            ...req.body
        })
        await syllabus.save();
        return res.json({
            message:'syllabus created successfull'
        })
    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

const uploadSyllabusFile=async(req,res)=>{
    try{
    
        const _id=req.params.id;
        const syllabus=await Syllabus.findById({_id});
        if(syllabus){

            if(syllabus.fileName){
                var fileName = syllabus.fileName;
                var filePath = path.join('./files/upload', fileName);
                fs.unlinkSync(filePath);
            }
           
                await Syllabus.findByIdAndUpdate(
                    { _id },
                    {
                        $set: { fileName: req.file.filename }
                    },
                    {
                        multi: true
                    }
                )
                res.json({
                    message: 'file upload successfully'
                })
           
        }
    }
    catch(err){
       
        res.json({err})
    }
}


const dawnloadSyllabusFile=async(req,res)=>{
    try{
        
      const className=req.params.className;
      const subjectName=req.params.subjectName;
        const syllabus=await Syllabus.findOne({subjectName,className});
        var fileName = syllabus.fileName;
        var filePath = path.join('./files/upload', fileName);
        if(syllabus){
          
            res.download(filePath,fileName,function(error){
                if(error){
                    res.json({
                        message:'file could not be downloaded'
                    })
                }
            });
            
        }
    }
    catch(err){
        console.log(err);
        res.json({err})
    }
}

const updateSyllabus=async(req,res)=>{
    try{
        const _id=req.params.id;
        await Syllabus.findByIdAndUpdate(
            {_id},{
                $set:req.body
            },
            {
                multi:true
            }
        )
        return res.json({
            message:'update successfully'
        })
    }
    catch(err){
        res.json({err});
    }
}

const deleteSyllabus=async(req,res)=>{
    try{
        const _id=req.params.id;

       const syllabus= await Syllabus.findById({_id});
        var fileName = syllabus.fileName;
        var filePath = path.join('./files/upload', fileName);
       if(syllabus){
           fs.unlinkSync(filePath);
           await Syllabus.findByIdAndDelete(_id);
           return res.json({
               message: 'delete successfully'
           })
       }
        
    }
    catch(err){
        console.log(err);
        res.json({err});
    }
}


const changePassword=async(req,res)=>{
    try {
        const _id=req.params.id;
        const {currentPassword,newPassword,confirmNewPassword}=req.body;

        const teacher=await Teacher.findById({_id});
        if(teacher){
            const comp=await bcrypt.compare(currentPassword,teacher.password);
            if(comp){
                const comp2=await bcrypt.compare(newPassword,teacher.password);
                if(comp2){
                    res.json({
                        message:'you are changing you current password to current password'
                    })
                }
               if(newPassword===confirmNewPassword){
                   const hashPassword = await bcrypt.hash(newPassword,10);
                   await Teacher.findByIdAndUpdate(
                       {_id},
                       {
                           $set:{password:hashPassword}
                       },
                       {
                           multi:true
                       }

                   )
                   res.json({
                       message:'password changed'
                   })
               }

            }
            else{
                res.json({
                    message:'password does not match'
                })
            }
        }
    } catch (error) {
        res.json({error})
    }
}



const forgotPassword=async(req,res)=>{
    try {
        const {email}=req.body;
        const teacher=await Teacher.findOne({email});
        if(!teacher){
            res.status(400).json({
                message:'user not found'
            })
        }

        const token=jwt.sign({_id:teacher._id},secreateKey,{expiresIn:'5m'});
        await Teacher.updateOne({resetLink:token});

        const Email=process.env.NM_EMAIL;
        const Pass=process.env.NM_EMAIL_PASS;


        //node mailer setup
        let transporter=nodemailer.createTransport({
            service:'gmail',
            auth:{
                user:Email,
                pass:Pass
            }
        })


        let mailOption={
            from: 'mdrabby7410@gmail.com',
            to: user.email,
            subject: 'Reset Password Link',
            text: token

        }


        transporter.sendMail(mailOption,function (err,data) {
            if(err){
                res.json({
                    message:'can not send mail',
                    err
                })
            }
            else{
                return res.json({
                    message:'mail send successfully'
                })
            }
        })


    } catch (error) {
        res.json({error})
    }
}



const resetPassword = async (req, res) => {
    try {
        const secrateKey = process.env.JWT_SECRETE
        const { token, newPassword } = req.body;
        var hashPassword = await bcrypt.hash(newPassword, 10);
        const isTrue = jwt.verify(token, secrateKey);

        if (isTrue) {
            const teacher = await Teacher.findOne({ resetLink: token });
            if (teacher) {
                await Teacher.updateOne({
                    password: hashPassword,
                    resetLink: ''
               })
                return res.json({
                    msg: 'Password Reset Successfully'
                })
            }
            else {
                res.json({
                    msg: 'Invalid token or expired'
                })
            }

        }
        else {
            res.json({
                msg: 'user not found fot this token'
            })
        }

    } catch (err) {
        res.json({ err })
    }
}


const viewAllSyllabus=async(req,res)=>{
    try{
        const {className}=req.body;
        const syllabus=await Syllabus.find({className});
        if(syllabus){
            res.json({
                Syllacuses:syllabus
            })
        }else{
            res.json({
                message:'syllabus not found'
            })
        }
    }
    catch(err){
        res.json({err})
    }
}


const viewAllStudent=async(req,res)=>{
    try{
        const { currentClass}=req.body;
        
        const student = await Student.find({ isDeleted:false,active:true,currentClass});
       
        if(student){
            res.json({
                student
            })
        }
        else{
            res.json({
                message:'student not found'
            })
        }
    }
    catch(err){
      
        res.json({err});
    }
}


const viewStudentInfo=async(req,res)=>{
    try{
        const{studentId}=req.body;
        const student=await Student.findOne({studentId});
        if(student){
            res.json({
                student
            })
        }
        else{
            res.json({
                message:'student not found'
            })
        }
    }
    catch(err){
        res.json({err})
    }
}

const teacherLogin=async(req,res)=>{
    try{
        const {teacherId,password}=req.body;
        let teacher = await Teacher.findOne({ teacherId});
        
        let data = {
            teacherId:teacher.teacherId
        }
        if (teacher) {
            let comp = await bcrypt.compare(password, teacher.password);
            const token = jwt.sign(data, secreateKey, { expiresIn: '1h' })
            if (comp) {
                res.status(200).json({
                    message: 'Login successfull',
                    token
                })
            }
            else {
                res.json({
                    message: 'password does not match'
                })
            }
        }
        else {
            res.json({
                message: 'User not found'
            })
        }
    }
    catch(err){
        console.log(err);
        res.json({err})
    }
}

module.exports={
    createSyllabus,
    uploadSyllabusFile,
    dawnloadSyllabusFile,
    updateSyllabus,
    deleteSyllabus,
    viewAllSyllabus,
    viewAllStudent,
    viewStudentInfo,
    teacherLogin,
    changePassword,
    forgotPassword,
    resetPassword
}