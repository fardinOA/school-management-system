const { 
    studentLogin,
    viewProfile,
    viewAllTeacher,
    viewSingleTeacher,
    viewClassRoutine,
    viewSyllabus,
    downloadSyllabus,
    changePassword,
    forgotPassword,
    resetPassword,
    updateProfilePic
}=require('../../controller/studentActivity/studentActivity');
const express=require('express');
const router=express.Router();

const auth=require('../../../middelware/auth');
const fileUploader=require('../../../middelware/updateProfilePic');

router.post('/login',studentLogin);
router.get('/view-profile',auth,viewProfile);
router.get('/view-all-teacher', auth,viewAllTeacher);
router.get('/view-single-teacher', auth,viewSingleTeacher);
router.get('/view-class-routine', auth,viewClassRoutine);
router.get('/view-syllabus',auth,viewSyllabus);
router.get('/syllabus/download/:subjectName/:className',auth,downloadSyllabus);
router.put('/change-password/:id', auth, changePassword);
router.post('/forgot-password', auth, forgotPassword);
router.post('/reset-password', auth, resetPassword);
router.post('/update-profile-pic/:id',fileUploader.single('image') ,updateProfilePic);

module.exports=router;