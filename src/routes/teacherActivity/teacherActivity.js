const {
    createSyllabus,
    uploadSyllabusFile,
    dawnloadSyllabusFile,
    updateSyllabus,
    deleteSyllabus,
    viewAllSyllabus,
    viewAllStudent,
    viewStudentInfo,
    teacherLogin,
    changePassword,
    forgotPassword,
    resetPassword
}=require('../../controller/teacherActivity/teacherActivity');

const fileUploader=require('../../../middelware/fileUploader')
const auth=require('../../../middelware/auth')
const express=require('express');
const router=express.Router();


router.post('/create-syllabus',auth,createSyllabus)
router.post('/upload-syllabus-file/:id',fileUploader.single('file'),uploadSyllabusFile);
router.get('/syllabus/download/:subjectName/:className', auth,dawnloadSyllabusFile);
router.put('/syllabus/update/:id', auth,updateSyllabus);
router.delete('/syllabus/delete/:id', auth,deleteSyllabus);
router.get('/view/all-syllabus', auth,viewAllSyllabus);
router.get('/view-all-student-by-class', auth,viewAllStudent);
router.get('/view-single-student', auth,viewStudentInfo);
router.post('/login',teacherLogin);
router.put('/change-password/:id', auth,changePassword);
router.post('/forgot-password',auth,forgotPassword);
router.post('/reset-password',auth,resetPassword);

module.exports=router;