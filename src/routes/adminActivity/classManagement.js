const {
    createClass,
    updateClass,
    deleteClass,
    viewAllClass
}=require('../../controller/adminActivity/classManagement');

const express=require('express');
const router=express.Router();



router.post('/class/create',createClass);
router.put('/class/update/:id',updateClass);
router.put('/class/delete/:id',deleteClass);
router.get('/class/view-all-class',viewAllClass);


module.exports=router;