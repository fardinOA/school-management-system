const {
    createNewTeacher,
    updateTeacherInfo,
    deleteTeacher,
    changeActiveInActiveStatus
}=require('../../controller/adminActivity/teacherManagement');

const express=require('express');
const router=express.Router();


router.post('/teacher/create',createNewTeacher);
router.put('/teacher/update/:id',updateTeacherInfo);
router.put('/teacher/delete/:id',deleteTeacher);
router.put('/teacher/:id/:value',changeActiveInActiveStatus)


module.exports=router;