const {
    createRoutine,
    updateRoutine,
    deleteRoutine,
    viewAllRoutine
}=require('../../controller/adminActivity/classRoutineManagement');
const express=require('express');
const router=express.Router();


router.post('/routine/create/:classId',createRoutine);
router.put('/routine/update/:classId/:routineId',updateRoutine);
router.delete('/routine/delete/:classId/:routineId',deleteRoutine);
router.get('/routine/view-all/:classId',viewAllRoutine);

module.exports=router;