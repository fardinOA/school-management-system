const {
    adminRegisterController,
    loginController,
    changePassword

}=require('../../controller/adminActivity/basicAdminActivity');

const express=require('express');
const router=express.Router();
const auth=require('../../../middelware/auth')

router.post('/signUp',adminRegisterController);
router.post('/login',loginController);
router.put('/change-password/:id',auth,changePassword);


module.exports=router;