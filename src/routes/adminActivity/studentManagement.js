cosnt= {
    AdmitSingleStudent,
    updateStudentInfo,
    deleteStudent,
    changeActiveInActiveStatus,
    viewProfile
}=require('../../controller/adminActivity/studenMangement');

const express=require('express');



const router=express.Router();




router.post('/student/admit-single-student',AdmitSingleStudent);
router.put('/student/update-info/:id',updateStudentInfo);
router.put('/student/delete/:id',deleteStudent);
router.put('/student/:id/:value',changeActiveInActiveStatus);
router.get('/student/view-profile/:id',viewProfile);


module.exports=router;