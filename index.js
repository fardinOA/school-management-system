const express=require('express');
require('dotenv').config();
const app=express();

const mongoose=require('mongoose');

app.use(express.json())

//all admin routes
const basicAdminActivityRoutes=require('./src/routes/adminActivity/basicAdminActivity');
const studentManagementRoutes=require('./src/routes/adminActivity/studentManagement');
const teacherManagementRoutes=require('./src/routes/adminActivity/teacherManagement');
const classManagementRouter=require('./src/routes/adminActivity/classManagement');
const classRoutineManagementRouter=require('./src/routes/adminActivity/classRoutineManagement');


// all teacher routes
const teacherActivityRouter=require('./src/routes/teacherActivity/teacherActivity')

// all student routes
const studentActivityRouter=require('./src/routes/studentActivity/studentActivity');


//mongodb connection
const url=process.env.MONGODB_URL

mongoose.connect(url,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
.then(console.log('Server connected'))
.catch(err=>{
    console.log(err);
})


app.use('/admin',basicAdminActivityRoutes,studentManagementRoutes,teacherManagementRoutes,classManagementRouter,classRoutineManagementRouter);
app.use('/teacher', teacherActivityRouter);
app.use('/student',studentActivityRouter);


const port=process.env.PORT || 5050

app.get('/',(req,res)=>{
    res.send(`<h1>I am Root</h1>`);
    
})

app.listen(port,console.log('Running on port 5050'))